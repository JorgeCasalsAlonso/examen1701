@extends('layouts.app')

@section('content')
	<h1>Alta de platos</h1>
	<form action="/dishes" method="POST">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="name">Nombre: </label>
			<input type="text" class="form-control" name="name" id="name" value="{{old('name')}}"/>
			@if ($errors->first('name'))
				<div class="alert alert-danger">
					<strong>¡Error!</strong> {{ $errors->first('name') }}
				</div>
			@endif
		</div>
		<div class="form-group">
			<label for="description">Descripci&oacute;n: </label>
			<textarea class="form-control" name="description" id="description">{{old('description')}}</textarea>
			@if ($errors->first('description'))
				<div class="alert alert-danger">
					<strong>¡Error!</strong> {{ $errors->first('description') }}
				</div>
			@endif
		</div>
		<div class="form-group">
			<label for="type">Tipo: </label>
			<select id="type" name="type_id" class="form-control">
				@foreach ($types as $type)
					<option value="{{$type->id}}" >
						{{$type->name}}
					</option>
				@endforeach
			</select>
			@if ($errors->first('type'))
				<div class="alert alert-danger">
					<strong>¡Error!</strong> {{ $errors->first('type') }}
				</div>
			@endif
		</div>
		<input type="hidden" id="user" name="user_id" value="{{ $user->id }}" />
		{{-- <!--<div class="form-group">
			@if (isset($logged_user))
				<input type="hidden" id="user" name="user_id" value="{{ $logged_user->id }}" />
			@else
				<label for="user">Usuario: </label>
				<select id="user" name="user_id" class="form-control">
					@foreach ($users as $user)
						<option value="{{$user->id}}"
							{{ old('user') == $user->id ? 'selected' : '' }}>
							{{$user->name}}
						</option>
					@endforeach
				</select>
				@if ($errors->first('user'))
					<div class="alert alert-danger">
						<strong>¡Error!</strong>{{ $errors->first('user') }}
					</div>
				@endif
			@endif
		</div>--> --}}
		<input type="submit" class="btn btn-primary" value="Dar de alta"/>
	</form>
@endsection
@extends('layouts.app')

@section('content')
	<h1>Listado de platos</h1>
	@can('create', 'App\Dish')
		<a href="/dishes/create">Nuevo plato</a>
	@else
		Inicie sesi&oacute;n para realizar altas
	@endcan
	<table class="table">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Descripci&oacute;n</th>
				<th>Tipo</th>
				<th>Usuario</th>
				<th/>
			</tr>
		</thead>
		<tbody>
			@foreach ($dishes as $dish)
				<tr>
					<td>{{ $dish->id }}</td>
					<td>{{ $dish->name }}</td>
					<td>{{ $dish->description }}</td>
					<td>{{ $dish->type->name }}</td>
					<td>{{ $dish->user->name }}</td>
					<td>
						<form method="post" action="/dishes/{{ $dish->id }}">
							@can('delete', $dish)
                        		<input type="hidden" name="_method" value="DELETE">
                        		{{ csrf_field() }}
                        		<input type="submit" value="Borrar">
                        	@endcan
                        	@can('update', $dish)
								<a href="/dishes/{{ $dish->id }}/edit">Editar</a>
							@endcan
							<a href="/dishes/{{ $dish->id }}"> Ver </a>
                        </form>
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
	@can('create', 'App\Dish')
		<a href="/dishes/create">Nuevo plato</a>
	@else
		Inicie sesi&oacute;n para realizar altas
	@endcan
	{{ $dishes->render() }} <br>
@endsection
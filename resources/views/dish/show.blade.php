@extends('layouts.app')

@section('content')

    <h1>Detalle del plato</h1>
    <p>ID: {{ $dish->id }} </p>
    <p>Nombre: {{ $dish->name }} </p>
    <p>Descripci&oacute;n: {{ $dish->description }} </p>
    <p>Tipo: {{ $dish->type->name }} </p>
    <p>Usuario: {{ $dish->user->name }} </p>
    <p>Ingredientes: 
    	<ul>
    		@foreach ($dish->ingredients as $ingredient)
    			<li>{{ $ingredient->name }}</li>
    		@endforeach
    	</ul>
    </p>

@endsection('content')
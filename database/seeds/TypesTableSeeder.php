<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            'name' => 'legumbres',
            ]);
        DB::table('types')->insert([
            'name' => 'pastas',
            ]);
        DB::table('types')->insert([
            'name' => 'sopas',
            ]);
        DB::table('types')->insert([
            'name' => 'ensaladas',
            ]);
        DB::table('types')->insert([
            'name' => 'carnes',
            ]);
        DB::table('types')->insert([
            'name' => 'pescados',
            ]);
        DB::table('types')->insert([
            'name' => 'postres',
            ]);
    }
}

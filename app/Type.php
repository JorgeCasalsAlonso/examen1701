<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public function dish()
    {
        return hasMany('dish', 'type_id');
    }
}

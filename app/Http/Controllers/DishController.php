<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Dish;
use App\Type;
use App\Ingredient;

class DishController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dishes = Dish::with('User')->with('Type')->paginate();
        return view('dish.index', ['dishes' => $dishes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Dish::class);
        $types = Type::get();
        $user = $request->user();
        //return view('dish.create', ['types' => $types, 'users' => $users, 'logged_user' => $user]);
        return view('dish.create', ['types' => $types, 'user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Dish::class);
        $this->validate($request, [
            'name' => 'required|max:40',
            'description' => 'required|max:300',
            'type_id' => 'required',
            'user_id' => 'required',
            ]);
        $dish = Dish::create($request->all());
        $dish->save();
        return redirect('/dishes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $dish = Dish::with('User')->with('Type')->with('Ingredients')->findOrFail($id);
        return view('dish.show', ['dish' => $dish]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = $request->user();
        $dish = Dish::findOrFail($id);
        $this->authorize('delete', $dish);
        $dish->ingredients()->detach();
        Dish::destroy($id);
        return redirect('/dishes');
    }
}
